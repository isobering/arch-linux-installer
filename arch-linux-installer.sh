#!/bin/bash
# Copyright (C) 2019 Ian Sobering
# Installs a basic Arch Linux system on a single disk.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Enforce unoffical bash strict mode
set -euo pipefail
IFS=$'\n\t'

ARCH_BLUE="\e[38;5;39m"
RED="\e[31m"
RESET="\e[0m"

SCRIPT_NAME="$(basename "$0")"
arguments=("$@")

# Device mapper name of the encrypted container
DM_NAME="luks"

# Volume group name containing logical volumes (LVs) for / and /swap
VG_NAME="vg00"

# Default swap size
SWAP_SIZE="4G"

# Region (used to set time zone, i.e. /usr/share/zoneinfo/[MY_REGION])
MY_REGION="America/Denver"

# Locale (to be uncommented in /etc/locale.gen)
MY_LOCALE="en_US.UTF-8 UTF-8"

# Language (used to set the LANG variable in /etc/locale.conf)
MY_LANG="en_US.UTF-8"

# Default hostname
MY_HOSTNAME="myhostname"

# Stuff to change in mkinitcpio.conf
MKINITCPIO_MODULES_OLD='MODULES=()'
MKINITCPIO_MODULES_NEW='MODULES=(ext4)'
MKINITCPIO_HOOKS_OLD='HOOKS=(base udev autodetect modconf block filesystems keyboard fsck)'
MKINITCPIO_HOOKS_NEW='HOOKS=(base udev autodetect modconf block encrypt lvm2 filesystems keyboard fsck)'

# Message from the script in pretty blue text
script_message() {
  tput bold; echo -e "${ARCH_BLUE}${SCRIPT_NAME}: ${1}${RESET}"; tput sgr0
}

# Script error message
script_error() {
  tput bold; echo -e "${RED}${SCRIPT_NAME}: ${1}${RESET}" 1>&2; tput sgr0
}

# Exit cleanup function
error_exit() {
  if [[ "$#" == 1 ]]; then script_error "$1"; fi
  script_message "Attempting to clean up"
  if [[ "${configure}" == true ]]; then
    script_message "Exiting chroot environment"; exit
  fi
  if [[ -f /mnt/root/${SCRIPT_NAME} ]]; then
    script_message "Deleting chroot copy of installation script"
    rm /mnt/root/${SCRIPT_NAME}
  fi
  script_message "Unmounting filesystems"; umount -R /mnt
  script_message "Turning off swap" swapoff -a
  script_message "Removing volume groups" vgchange -an; vgremove -ff ${VG_NAME}
  script_message "Closing encrypted container"; cryptsetup luksClose ${DM_NAME}
}


# Print help and usage information
print_help() {
  cat <<EOF
Usage: ${SCRIPT_NAME} [OPTION...] /dev/sdX

Installs a basic Arch Linux system on a single disk.

Options:

  -h, --help            Print this help
  -p, --package-list    Specify a package list file
       
Examples:
  
Install Arch Linux on target disk /dev/sdX
  ${SCRIPT_NAME} -p my_packages.txt /dev/sdX

EOF
}

# Verify the boot mode
verify_boot_mode() {
  script_message "Verifying boot mode"
  if [[ ! -d /sys/firmware/efi/efivars ]]; then
    script_error "the /sys/firmware/efi/efivars directory does not exist"
    echo "The system may be booted in BIOS or CSM mode"
    echo "Reboot the system in UEFI mode before continuing"
  exit 1
  fi
}

# Update the system clock
update_system_clock() {
  script_message "Updating the system clock"
  timedatectl set-ntp true 
}

# Partition the disks
partition_disk() {
  script_message "Partitioning disk"
  parted -s -a optimal ${DEVICE_ID} -- mklabel gpt \
  mkpart primary fat32 1MiB 551MiB \
  set 1 esp on \
  mkpart primary ext4 551MiB 100%
}

# Format the partitions
setup_luks() {
  script_message "Creating the encrypted container"
  cryptsetup --type luks --cipher aes-xts-plain64 --key-size 512 --hash sha512 --iter-time 5000 --use-random --verify-passphrase luksFormat ${LUKS_PARTITION}
  script_message "Opening the encrypted container"
  cryptsetup open ${LUKS_PARTITION} ${DM_NAME}
}

# Setup LVM on the encrypted container
setup_lvm() {
  script_message "Creating physical volumes"
  pvcreate /dev/mapper/${DM_NAME}
  script_message "Creating volume groups"
  vgcreate ${VG_NAME} /dev/mapper/${DM_NAME}
  script_message "Creating logical volumes"
  lvcreate -n swap -L "${SWAP_SIZE}" ${VG_NAME}
  lvcreate -n root -l 100%FREE "${VG_NAME}"
}

# Create file systems
create_filesystems() {
  script_message "Creating file systems"
  mkfs.fat -F32 ${BOOT_PARTITION}
  mkfs.ext4 -L root /dev/mapper/${VG_NAME}-root
  mkswap -L swap /dev/mapper/${VG_NAME}-swap
}

# Mount file systems
mount_filesystems() {
  script_message "Mounting file systems"
  mount /dev/mapper/${VG_NAME}-root /mnt
  mkdir /mnt/boot
  mount ${BOOT_PARTITION} /mnt/boot
  swapon /dev/mapper/${VG_NAME}-swap
}

# Select the mirrors
select_mirrors() {
  # TODO: Implement with reflector and setup reflector service
  script_message "TODO: Implement mirror ranking with reflector"
}

# Install packages
install_packages() {
  local package_list=()
  script_message "Installing packages"
  readarray package_list < ${PACKAGE_LIST_FILE}
  pacstrap /mnt ${package_list[@]}
}

# Generate an fstab file
generate_fstab() {
  script_message "Generating an fstab file"
  genfstab -p /mnt >> /mnt/etc/fstab
}

# Change root into the new system
change_root() {
  script_message "Changing root into the new system"
  cp "$0" /mnt/root
  arch-chroot /mnt /root/$(basename "$0") --configure ${arguments[@]}
}

# Set time zone
set_time_zone() {
  script_message "Setting the time zone"
  ln -sf /usr/share/zoneinfo/${MY_REGION} /etc/localtime
}

# Localization
generate_localizations() {
  script_message "Uncommenting localizations"
  sed -i 's/#${MY_LOCALE}/${MY_LOCALE}/' /etc/locale.gen
  script_message "Generating localizations"
  locale-gen
  script_message "Setting the LANG variable in /etc/locale.conf"
  echo LANG=${MY_LANG} > /etc/locale.conf
}

# Set the hostname and configure the hosts file
set_hostname() {
  script_message "Setting the hostname"
  echo "${MY_HOSTNAME}" > /etc/hostname
  script_message "Adding corresponding entries to /etc/hosts"
  cat > /etc/hosts <<EOF
127.0.0.1	localhost
::1		localhost
127.0.1.1	${MY_HOSTNAME}.localdomain	${MY_HOSTNAME}
EOF
}

# Configure mkinitcpio.conf and recreate the initramfs image
recreate_initramfs() {
  script_message "Configuring mkinitcpio.conf"
  sed -i "s/${MKINITCPIO_MODULES_OLD}/${MKINITCPIO_MODULES_NEW}/g" /etc/mkinitcpio.conf
  sed -i "s/${MKINITCPIO_HOOKS_OLD}/${MKINITCPIO_HOOKS_NEW}/g" /etc/mkinitcpio.conf
  script_message "Recreating the initramfs image"
  mkinitcpio -p linux
}

# Set the root password
set_root_password() {
  script_message "Setting the root password"
  passwd
}

# Start services
enable_services() {
  script_message "Enabling services"
  systemctl enable fstrim.timer
  systemctl enable NetworkManager.service
  systemctl enable gdm.service
}

# Install the bootloader
install_bootloader() {
  local part_uuid
  part_uuid="$(blkid -s PARTUUID -o value ${LUKS_PARTITION})"
  script_message "Installing the boot loader"
  bootctl --path=/boot install || error_exit
  script_message "Configuring /boot/loader/loader.conf"
  cat > /boot/loader/loader.conf <<EOF
#timeout 3
#console-mode keep
default	arch*
editor	no
EOF

  script_message "Creating /boot/loader/entries/arch_linux.conf"
  cat > /boot/loader/entries/arch_linux.conf <<EOF
title Arch Linux
linux /vmlinuz-linux
initrd /intel-ucode.img
initrd /initramfs-linux.img
options	cryptdevice=PARTUUID=${part_uuid}:${DM_NAME}:allow-discards root=/dev/mapper/${VG_NAME}-root quiet rw
EOF
}

# Unmount file systems and reboot
unmount_filesystems() {
  script_message "Unmounting file systems"
  umount -R /mnt
  swapoff -a
  script_message "Installation complete"
}

# Perform pre-installation tasks, install packages, and change root
install_arch_linux() {
  verify_boot_mode
  update_system_clock
  partition_disk
  setup_luks
  setup_lvm
  create_filesystems
  mount_filesystems
  select_mirrors
  install_packages
  generate_fstab
  change_root # Script will resume here when exiting chroot environment
  unmount_filesystems
}

# Install additional packages and configure the installation
configure_arch_linux() {
  set_time_zone
  generate_localizations
  set_hostname
  recreate_initramfs
  set_root_password
  enable_services
  install_bootloader
  script_message "exiting the chroot environment"; exit
}

main() {
  # Parse option arguments with getopt
  configure=false
  options="$(getopt -o h --long configure,help,hostname:,package-list:,swap:,target: -n ${SCRIPT_NAME} -- "$@")"
  eval set -- "${options}"
  while true; do
    case $1 in
      --configure) configure=true; shift ;;
      -h | --help) print_help; exit 0 ;;
      --hostname) MY_HOSTNAME=$2; shift 2 ;;
      --package-list) PACKAGE_LIST_FILE=$2; shift 2 ;;
      --swap) SWAP_SIZE=$2; shift 2 ;;
      --target) DEVICE_ID=$2; shift 2 ;;
      --) shift; break ;;
      *) break ;;
    esac
  done
  
  #
  BOOT_PARTITION="${DEVICE_ID}1"
  LUKS_PARTITION="${DEVICE_ID}2"
  
  # Install Arch Linux
  if [[ "${configure}" == "false" ]]; then
    install_arch_linux # Perform configuration steps in chroot environment
  elif [[ "${configure}" == "true" ]]; then
    configure_arch_linux # Perform installation tasks
  fi
}
main "$@"