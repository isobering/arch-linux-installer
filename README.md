# arch-linux-installer
Installs a simple Arch Linux system on a single disk.
## Usage
After booting into the Arch Linux live environment, download and run the
installation script by running
```shell
# curl https://gitlab.com/isobering/arch-linux-installer/raw/master/arch-install > arch-linux-installer
# sh arch-linux-installer [options] /dev/sdX
```
For help and usage information, run
```shell
# sh arch-linux-installer --help
```
## How it Works
This script automates the installation process given in the
[Arch Linux Installation Guide](https://wiki.archlinux.org/index.php/Installation_guide).

This script deviates from the basic Arch Linux Installation Guide in two ways:

1. The root partition is encrypted using dm-crypt
2. The installation uses
   [Logical Volume Management (LVM)](https://wiki.archlinux.org/index.php/LVM)
   instead of regular partitions.

### Installed Packages
The script installs the following packages in addition to the `base` package group:

- `base-devel` - Tools for building (compiling and linking)
- `bash-completion` - Programmable completion for the bash shell
- `firefox` - Popular open-source graphical web browser from [Mozilla](https://www.mozilla.org)
- `gnome` - Package group containing the GNOME desktop environment and other applications
- `intel-ucode` - Microcode for Intel CPUs
- `nvidia` - Proprietary NVIDIA graphics driver
- `nvidia-utils` - NVIDIA graphics driver utilities
- `util-linux` - Random collection of Linux utilities
- `sudo` - Execute a command as another user

These packages provide a basic Arch Linux system with a user-friendly desktop
environment.

### Suspend and Hibernation
If the target system uses hibernation (suspend to disk), use the `--hibernate` argument:
```shell
# ./arch-linux-installer --hibernate /dev/sdX
```
The script determines how much swap space the target system requires based on
the `--hibernate` argument and the total amount of available physical memory.
The swap size calculation is based on rules of thumb and worked examples
in the [Ubuntu SwapFAQ Community Help discussion](https://help.ubuntu.com/community/SwapFaq).
Please note that the Ubuntu article has some errors - the rules of thumb described
in the article will not produce the results presented in the worked examples.

For systems with 1 GiB of physical memory or less:

- With `--hibernate`, swap size is set equal to twice the amount of RAM
- Otherwise, swap size is set equal to the amount of RAM

For systems with greater than 1 GiB of physical memory:

- With `--hibernate`, swap size is set equal to RAM + round(sqrt(RAM))
- Otherwise, swap size is set equal to round(sqrt(RAM))

These rules of thumb may be overly conservative for systems that use hibernation.
See [the Arch Linux Wiki article on swap](https://wiki.archlinux.org/index.php/Swap)
for a discussion on how swap works and how to improve swap performance.

Use the `--swap` argument to manually specify swap size.
```shell
# sh arch-linux-installer --swap 2G /dev/sdX
```

The `--swap` argument overrides